#include <cassert>
#include <iostream>
#include <string>
#include <unordered_map>
#include <zmqpp/zmqpp.hpp>
#include <unordered_set>
#include "portaudio.h"
#include "Util.hpp"
#include <thread>
#include <unistd.h>

using namespace std;
using namespace zmqpp;

class User {
private:
  string name;
  string password;
  string netId;
  bool connected;
  list<string> contacts;

public:
  User() {}
  User(const string &name, const string &pwd, const string &id)
      // Attribute initialization
      : name(name),
        password(pwd),
        netId(id),
        connected(false) {}
  bool isPassword(const string &pwd) const { return password == pwd; }
  void connect(const string &id) {
    connected = true;
    netId = id;
  }

  const string &identity() const { return netId; }

};

class ServerState {
private:
  // Connected users
  unordered_map<string, User> users;
  unordered_map<string, unordered_set<string> > Group;
  socket &sckt;

public:
  ServerState(socket &s) : sckt(s) {}


  void send(message &m) { sckt.send(m); }


  void data_error(const string &id) {
      message m;
      m << id << "Numero de argumentos invalidos";
      send(m);
    }

  void newUser(const string &name, const string &pwd, const string &id) {
    // TODO: check that the user does not exist.

    if(users.count(name) > 0){

      message m;
      m << id << "Este usuario ya esta ocupado intente otro";
      send(m);

    } else{
      users[name] = User(name, pwd, id);
      message m;
      m << id << "Su usuario a sido ingresado con exito";
      send(m);
    }
  }
  void newGroup(const string &name , const string &id){
    // TODO: check that the user does not exist.

    if(Group.count(name) > 0){

      message m;
      m << id << "El nombre de este Grupo ya esta ocupado intente otro";
      send(m);

    } else{

      string origen;

      for(auto it = users.begin(); it != users.end(); it++){

        if(it->second.identity() == id){
          origen = it->first;
          break;
        }
      }

      Group[name].insert(origen);

      message m;
      m << id << "Su grupo se a creado con exito";
      send(m);
    }

  }

  void joinGroup(const string &name_group, const string &id) {
    // TODO: check that the user does not exist.

      string origen;

      for(auto it = users.begin(); it != users.end(); it++){

        if(it->second.identity() == id){
          origen = it->first;
          break;
        }
      }


    if(Group[name_group].count(origen) > 0){

      string text;

      text += "Ud ya es miembre del grupo: " + name_group;

      message m;

      m << id << text;

      send(m);

    } else{
      Group[name_group].insert(origen);

      string text;

      text += "Ud fue agregado al grupo: " + name_group;

      message m;

      m << id << text;

      send(m);

    }

  }

  bool login(const string &name, const string &pwd, const string &id) {

    if (users.count(name) > 0) {

      bool ok = users[name].isPassword(pwd);

      if (ok){

		users[name].connect(id);
		message m;

  		string text;
  		text += "User " + name + " joins the chat server";

    	m << id << text;

    	send(m);

      }else{

	  	message m;

	  	string text;
	  	text = "Wrong password";

	    m << id << text;

	    send(m);

      }

    }else{

	  	message m;

	  	string text;
	  	text = "Wrong user o user no Register";

	    m << id << text;

	    send(m);
	}

  }

  void sendMessage(const string &dest, const string &text, const string &sender) {

    if(users.count(dest) > 0){

      string origen;

      for(auto it = users.begin(); it != users.end(); it++){

        if(it->second.identity() == sender){
          origen = it->first;
          break;
        }
      }
      string text2;
      text2 = origen+": "+text;

      message m;
      m << users[dest].identity() << "Mensaje" << text2;
      send(m);

    } else if(Group.count(dest) > 0){

      string origen;

      for(auto it = users.begin(); it != users.end(); it++){

        if(it->second.identity() == sender){
          origen = it->first;
          break;
        }
      }
      string text2;
      text2 = origen+": "+text;

      for(auto it = Group[dest].begin() ; it != Group[dest].end(); it++){

        if(*it != origen){
          message m;
          m << users[*it].identity() << "Mensaje" << text2;
          send(m);
        }

      }

    } else {

    	message m;

    	m << sender << "El mensaje fallo, el grupo o usuario no existe";

    	send(m);
    }
  }
  void sendVideo(const string &dest,const vector <int16_t>& v,unsigned int channelCount,
                const uint64_t SoundSampleCount, const unsigned int SoundSampleRate ,
                message &msg,const string &sender){

    if(users.count(dest) > 0){
      string origen;
      for(auto it = users.begin(); it != users.end(); it++){
        if(it->second.identity() == sender){
          origen = it->first;
          break;
        }
      }
      message m;
      string text2;
      int tam;
      m << users[dest].identity() << "Video" << origen << channelCount;
      m << SoundSampleCount << SoundSampleRate << v;

      m << tam;
      for (int i = 0; i < tam; i++){
      	msg >> text2;
        m << text2;
      }
      send(m);
    } else if(Group.count(dest) > 0){
      string origen;
      for(auto it = users.begin(); it != users.end(); it++){
        if(it->second.identity() == sender){
          origen = it->first;
          break;
        }
      }

      int tam;
      msg >> tam;
      vector<string> s;
      string text2;

      for (int i = 0; i < tam; i++){
        msg >> text2;
        s.push_back(text2);
       }

      for(auto it = Group[dest].begin() ; it != Group[dest].end(); it++){
        if(*it != origen){
          message m;

  	      int tam;
          m << users[*it].identity() << "Video" << origen << channelCount;
          m << SoundSampleCount << SoundSampleRate << v;

          int tam2;
          tam2 = s.size();

          m << tam2;

           for (int i = 0; i < tam2; i++){
     	      m << s[i];
 	         }
           cout << "Perrito" << endl;
           send(m);
         }
      }
    } else{

    	message m;

    	m << sender << "El video fallo, el grupo o usuario no existe";

    	send(m);

    }
  }
  void sendSound(const string &dest,const vector <int16_t>& v,unsigned int channelCount,
              const uint64_t SoundSampleCount, const unsigned int SoundSampleRate , const string &sender){
    if(users.count(dest) > 0){
      string origen;
      for(auto it = users.begin(); it != users.end(); it++){
        if(it->second.identity() == sender){
          origen = it->first;
          break;
        }
      }
      message m;
      m << users[dest].identity() << "Audio" << origen << channelCount << SoundSampleCount << SoundSampleRate;
      m << v;
      send(m);

    } else if(Group.count(dest) > 0){

      string origen;
      for(auto it = users.begin(); it != users.end(); it++){
        if(it->second.identity() == sender){
          origen = it->first;
          break;
        }
      }

      for(auto it = Group[dest].begin() ; it != Group[dest].end(); it++){
        if(*it != origen){
          message m;
          m << users[*it].identity() << "Audio" << origen << channelCount << SoundSampleCount << SoundSampleRate << v;
          //threads.push_back(thread(&ServerState::sendSoundGroup,this,ref(m)));
          send(m);
        }
      }

    } else {

        message m;

    	m << sender << "El audio fallo, el grupo o usuario no existe";

    	send(m);
    }
  }


};


//Declaración de funciones
void login(message &msg, const string &sender, ServerState &server);
void dispatch(message &msg, ServerState &server);
void newUser(message &msg, const string &sender, ServerState &server);
void sendMessage(message &msg, const string &sender, ServerState &server);
void newGroup(message &msg, const string &sender, ServerState &server);
void joinGroup(message &msg, const string &sender, ServerState &server);

int main(int argc, char *argv[]) {
  context ctx;
  socket s(ctx, socket_type::xreply);
  s.bind("tcp://*:4242");

  ServerState state(s);
  while (true) {

    message req;

    s.receive(req);

    dispatch(req, state);
  }
  cout << "Finished." << endl;
}


void sendMessage(message &msg, const string &sender, ServerState &server) {
  string dest;
  msg >> dest;

  string text;
  msg >> text;
  server.sendMessage(dest, text, sender);
}
void sendSound(message &msg, const string &sender, ServerState &server){
  //cout << "llegue aca" << endl;
  string dest;
  msg >> dest;

  unsigned int channelCount;
  msg >> channelCount;

  uint64_t SoundSampleCount;
  msg >> SoundSampleCount;
  //cout << "recv: " << SoundSampleCount << endl;

  unsigned int SoundSampleRate;
  msg >> SoundSampleRate;

  vector <int16_t> v;
  msg >> v;

  server.sendSound(dest,v, channelCount,SoundSampleCount,SoundSampleRate,sender);
}
void sendVideo(message &msg, const string &sender, ServerState &server) {

  string dest;
  msg >> dest;

  unsigned int channelCount;
  msg >> channelCount;

  uint64_t SoundSampleCount;
  msg >> SoundSampleCount;

  unsigned int SoundSampleRate;
  msg >> SoundSampleRate;

  vector <int16_t> v;
  msg >> v;

  server.sendVideo(dest,v,channelCount,SoundSampleCount,SoundSampleRate,
                    msg, sender);
}

void login(message &msg, const string &sender, ServerState &server) {

  string userName;
  msg >> userName;
  string password;
  msg >> password;

  server.login(userName, password, sender);

}

void newUser(message &msg, const string &sender, ServerState &server){

  string userName;
  msg >> userName;

  string password;
  msg >> password;

  server.newUser(userName, password, sender);

}

void newGroup(message &msg, const string &sender, ServerState &server){

  string nameGroup;
  msg >> nameGroup;

  server.newGroup(nameGroup, sender);

}

void joinGroup(message &msg, const string &sender, ServerState &server){

  string nameGroup;
  msg >> nameGroup;

  server.joinGroup(nameGroup, sender);

}

void dispatch(message &msg, ServerState &server) {

  assert(msg.parts() > 2);

  string sender;
  msg >> sender;

  string action;
  msg >> action;

  int tam;
  msg >> tam;


  if (action == "login") {

  	if(tam == 3){

    	login(msg, sender, server);

    } else{

    	server.data_error(sender);

    }

  } else if(action == "Register"){

  	if(tam == 3){

    newUser(msg, sender, server);

    } else{

      server.data_error(sender);

    }

  }else if (action == "msg") {

  	if(tam == 2){

     	sendMessage(msg, sender, server);

    } else {

	  server.data_error(sender);

    }


  } else if(action == "Crear_Grupo"){

  	if(tam == 2){

    	newGroup(msg, sender, server);

	} else {
		server.data_error(sender);
	}


  } else if(action == "Unirse_Grupo"){

  	if(tam == 2){

   		joinGroup(msg, sender, server);

	} else{

		server.data_error(sender);
	}

  }else if(action == "Recording"){


    	sendSound(msg,sender,server);


  }else if(action == "Video"){

    	sendVideo(msg,sender,server);

  }else {

    cerr << "Action not supported/implemented for" << action << endl;

    message reply;
   	string respuesta;

   	respuesta += "Action not supported "+ action;
    reply << sender << respuesta;
    server.send(reply);
  }

}
