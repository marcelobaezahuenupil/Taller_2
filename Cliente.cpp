#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <zmqpp/zmqpp.hpp>
#include <sstream>
#include <SFML/Graphics.hpp>
#include "portaudio.h"
#include <unistd.h>
#include <iostream>
#include <SFML/Audio.hpp>
#include <fstream>
#include "Util.hpp"
#include <thread>
#include <unistd.h>
#include <functional>
#include <queue>
#include <cassert>
#include <sstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace zmqpp;

vector<string> tokenize(string &input) {
  stringstream ss(input);
  vector<string> result;
  string s;
  while (ss >> s)
    result.push_back(s);
  return result;
}
void Video(socket &s,const string action,const string dest){
          message m;
          cout << "Pase aca..." << endl;
          cv::VideoCapture input_cap(0);
          if (!input_cap.isOpened()) {
            std::cout << "!!! Input video could not be opened1" << std::endl;
          }
          // Setup output video
          cv::VideoWriter output_cap("video.avi",
                                     input_cap.get(CV_CAP_PROP_FOURCC),
                                     input_cap.get(CV_CAP_PROP_FPS),
                                     cv::Size(input_cap.get(CV_CAP_PROP_FRAME_WIDTH),
                                              input_cap.get(CV_CAP_PROP_FRAME_HEIGHT)));

          cout << "Pase aca..." << endl;
          if (!output_cap.isOpened()) {
            std::cout << "!!! Output video could not be opened2" << std::endl;
          }
          // Loop to read from input and write to output
          cv::Mat frame;
          int i = 0;
          sf::SoundBufferRecorder recorder;
          cout << "Comenzo a grabar" << endl;
          recorder.start();
          while(i < 100){
            if (!input_cap.read(frame))
              break;
            output_cap.write(frame);
            i++;
            //cout << i << endl;
          }
          recorder.stop();
          sf::SoundBuffer buffer;
          buffer = recorder.getBuffer();
          vector<int16_t> SoundSample(buffer.getSamples(), buffer.getSamples() + buffer.getSampleCount());
          const unsigned int channelCount=buffer.getChannelCount();
          const uint64_t SoundSampleCount=buffer.getSampleCount();//tamaño
          const unsigned int SoundSampleRate=buffer.getSampleRate();

          input_cap.release();
          output_cap.release();
          ifstream video("video.avi");
          string cadena;
          vector<string> v;
          while(getline(video, cadena)){
            v.push_back(cadena);
          }
          video.close();
          message msg;
          int tamano;
          tamano = v.size();
          msg << action;
          msg << 3;
          msg << dest;
          msg << channelCount;
          msg << SoundSampleCount;
          msg << SoundSampleRate;
          msg << SoundSample;
          msg << tamano;
          for(int k = 0 ; k < v.size() ; k++){
            msg << v[k];
          }
          s.send(msg);

}
void music(bool& grab,socket& s,string action, string dest){
  cout << "Recording began" << endl;
  while(grab){
    message m;
    sf::SoundBufferRecorder recorder;
    recorder.start();
    usleep(5000000);//5 segundos...
    recorder.stop();
    sf::SoundBuffer buffer;
    buffer = recorder.getBuffer();
    vector<int16_t> SoundSample(buffer.getSamples(), buffer.getSamples() + buffer.getSampleCount());
    const unsigned int channelCount=buffer.getChannelCount();
    const uint64_t SoundSampleCount=buffer.getSampleCount();//tamaño
    const unsigned int SoundSampleRate=buffer.getSampleRate();
    m << action;
    m << 3;
    m << dest;
    m << channelCount;
    m << SoundSampleCount;
    m << SoundSampleRate;
    m << SoundSample;
    s.send(m);
  }
  cout << "Recording stop" << endl;


}

int main(int argc, char const *argv[]) {
  if (argc != 5) {
    cerr << "Invalid arguments" << endl;
    return EXIT_FAILURE;
  }


  string address(argv[1]);
  string operacion(argv[2]);
  string userName(argv[3]);
  string password(argv[4]);
  string sckt("tcp://");
  sckt += address;

  context ctx;
  socket s(ctx, socket_type::xrequest);

  cout << "Connecting to: " << sckt << endl;
  s.connect(sckt);
  //Audio
  queue <string> QOrigin;
  queue < vector<int16_t> > QSoundSample;
  queue < unsigned int > QchannelCount;
  queue < uint64_t > QSoundSampleCount;
  queue < unsigned int > QSoundSampleRate;

  //Video
  queue <string> QOriginV;
  queue < vector<int16_t> > QSoundSampleV;
  queue < unsigned int > QchannelCountV;
  queue < uint64_t > QSoundSampleCountV;
  queue < unsigned int > QSoundSampleRateV;

  message login;
  login << operacion << 3 << userName << password;
  s.send(login);
  thread x1;
  int console = fileno(stdin); //fileno es donde C reconoce la terminal
  poller poll; //Un objeto que estara escuchando varias fuentes
  poll.add(s, poller::poll_in);
  poll.add(console, poller::poll_in);

  while (true) {
      bool gra=true;
      //cout << "llegue acaaaa" << endl;
      if (poll.poll()) { // There are events in at least one of the sockets
        if (poll.has_input(s)) {
          // Handle input in socket
          message m;
          s.receive(m);
          string action;
          m >> action;
          if(action=="Video"){
            cout << "Entro aca" << endl;
            string origin;
            //cout << "Se encolo audio.." << endl;
            vector <int16_t> SoundSample;
            unsigned int channelCount;
            uint64_t SoundSampleCount;
            unsigned int SoundSampleRate;
            m >> origin;
            m >> channelCount;
            m >> SoundSampleCount;
            m >> SoundSampleRate;
            m >> SoundSample;
            cout << "Aca voy!!" << endl;
            cout << origin << endl;
            QOriginV.push(origin);
            QchannelCountV.push(channelCount);
            QSoundSampleCountV.push(SoundSampleCount);
            QSoundSampleRateV.push(SoundSampleRate);
            QSoundSampleV.push(SoundSample);
            while(!QOriginV.empty()){
              cout << "Aca voy!!" << endl;
              //Comienza a Sonar el video....
              sf::SoundBuffer buffer;
              buffer.loadFromSamples(QSoundSampleV.front().data(),
                    QSoundSampleCountV.front(),QchannelCountV.front(),
                    QSoundSampleRateV.front());
              sf::Sound sound(buffer);
              cout << "Aca voy!!" << endl;
              cout << origin << " Play:" << endl;
              int tam;
            	string cadena;
            	m >> tam;
              cout << "Aca voy!!" << endl;
            	ofstream out("Video_copy.avi");
              cout << "Descargando Video" << endl;
              cout << "Aca voy!!" << endl;
            	for(int k = 0 ; k < tam ; k++){
            		m >> cadena;
            		out << cadena << endl;
            	}
            	cout << "Se descargo correctamente el video" << endl;
              sound.play();

              int key = 0;

              CvCapture* capture = cvCaptureFromAVI("Video_copy.avi");
              IplImage* frame = cvQueryFrame( capture );

              // Check
              if ( !capture )
              {
                  fprintf( stderr, "Cannot open AVI!\n" );
                  return 1;
              }

              // Get the fps, needed to set the delay
              int fps = ( int )cvGetCaptureProperty( capture, CV_CAP_PROP_FPS );

              // Create a window to display the video
              cvNamedWindow( "video", CV_WINDOW_AUTOSIZE );

              while( key != 'x' )
              {
                  // get the image frame
                  frame = cvQueryFrame( capture );

                  // exit if unsuccessful
                  if( !frame ) break;

                  // display current frame
                  cvShowImage( "video", frame );

                  // exit if user presses 'x'
                  key = cvWaitKey(45);
              }

              // Tidy up
              cvDestroyWindow( "video" );
              cvReleaseCapture( &capture );

              usleep(10000000);//se duerme 10 segundo, la duracion del video...
              sound.stop();
              QOriginV.pop();
              QchannelCountV.pop();
              QSoundSampleCountV.pop();
              QSoundSampleRateV.pop();
              QSoundSampleV.pop();
            }
          }
          if (action=="Audio"){
            string origin;
            cout << "Se encolo audio.." << endl;
            vector <int16_t> SoundSample;
            unsigned int channelCount;
            uint64_t SoundSampleCount;
            unsigned int SoundSampleRate;
            m >> origin;
            m >> channelCount;
            m >> SoundSampleCount;
            m >> SoundSampleRate;
            m >> SoundSample;
            QOrigin.push(origin);
            QchannelCount.push(channelCount);
            QSoundSampleCount.push(SoundSampleCount);
            QSoundSampleRate.push(SoundSampleRate);
            QSoundSample.push(SoundSample);
            while(!QOrigin.empty()){

              sf::SoundBuffer buffer;
              buffer.loadFromSamples(QSoundSample.front().data(),
                    QSoundSampleCount.front(),QchannelCount.front(),
                    QSoundSampleRate.front());
              sf::Sound sound(buffer);
              cout << QOrigin.front() << " Play:" << endl;
              sound.play();
              usleep(5000000);
              sound.stop();
              QOrigin.pop();
              QchannelCount.pop();
              QSoundSampleCount.pop();
              QSoundSampleRate.pop();
              QSoundSample.pop();
            }

          }
          else if (action=="Mensaje"){
            string response;
            m >> response;
            cout << "Socket> " << response << endl;

          } else{

          	cout  << action << endl;

          }
        }
        if (poll.has_input(console)) {
          // Handle input from console
          string input;
          getline(cin, input);
          vector<string> tokens = tokenize(input);
          message m;
          if(tokens[0] == "msg"){
            m << tokens[0];

            int tam;
            tam = tokens.size();
            m << tam;
            m << tokens[1];
            string str = "";
            for(int i = 2 ; i < tokens.size(); i++){
              str = str+" "+tokens[i];
            }
            m << str;
            s.send(m);
          }else if (tokens[0] =="Recording"){
            string action=tokens[0];
            string dest=tokens[1];
            x1 =thread(music,ref(gra),ref(s),action,dest);//comienzan a grabar...
          }else if (tokens[0]=="StopRec"){
            gra=false;
            x1.join();
          }else if(tokens[0]=="Video"){
            string action=tokens[0];
            string dest=tokens[1];

            if(tokens.size() == 2){
            	Video(s,action,dest);
            } else{

            	cout << "Numero de argumentos invalidos" << endl;

            }

          }
          else{

          	m << tokens[0];
          	int tam;
          	tam = tokens.size();
          	m << tam;
            for(int i = 1 ; i < tokens.size(); i++){
              m << tokens[i];
            }
            s.send(m);
          }
        }
      }

  }
  return EXIT_SUCCESS;
}
