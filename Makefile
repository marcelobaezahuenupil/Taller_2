#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/rin/zmq/lib
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/rin/opencv/release/lib
ZMQ=/home/rin/zmq
CC=g++ -std=c++11 -I$(ZMQ)/include -L$(ZMQ)/lib -I/home/rin/opencv/include/opencv -I/home/rin/opencv/include/opencv2 -L/home/rin/opencv/release/lib

all: client server

client: Cliente.cpp
	$(CC) -o client Cliente.cpp -pthread -lzmq -lzmqpp -lsfml-audio -lopencv_videoio -lopencv_core -lopencv_highgui

server: server.cc
	$(CC) -o server server.cc -pthread -lzmq -lzmqpp -lsfml-audio
